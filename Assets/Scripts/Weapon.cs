﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    public String[] targetTags;
    public float baseDamage = 25f;
    public float poweredDamageMultiplier = 3.0f;

    Collider weaponCollider;
    List<Damageable> insideCollider = new List<Damageable>();

    // Use this for initialization
    void Start() {
        weaponCollider = GetComponent<Collider>();
    }

    public void PoweredAttack()
    {
        Attack(baseDamage * poweredDamageMultiplier);
    }

    public void Attack()
    {
        Attack(baseDamage);
    }

    public void Attack(float damage)
    {
        foreach (Damageable damageable in insideCollider)
        {
            if (!damageable.IsDead)
            {
                damageable.TakeDamage(transform.parent.gameObject, damage);
            }
        }
        insideCollider.RemoveAll(elem => elem.IsDead);
    }

    //called when something enters the trigger
    private void OnTriggerEnter(Collider other)
    {
        // Ignore triggers...
        if (!other.isTrigger)
        {
            Damageable otherDamageable = other.GetComponent<Damageable>();
            if (otherDamageable != null && !otherDamageable.IsDead && HasTag(other, targetTags) && !insideCollider.Contains(otherDamageable))
            {
                Debug.Log("Enter in weapon trigger: " + other.gameObject.name);
                insideCollider.Add(otherDamageable);
            }
        }
    }

    private bool HasTag(Collider other, String[] tags)
    {
        foreach(String tag in targetTags)
        {
            if (other.CompareTag(tag))
            {
                return true;
            }
        }
        return false;
    }

    private void OnTriggerExit(Collider other)
    {
        Damageable otherDamageable = other.GetComponent<Damageable>();
        if (otherDamageable != null && insideCollider.Contains(otherDamageable))
        {
            insideCollider.Remove(otherDamageable);
        }
    }

    public bool CanAttack()
    {
        return insideCollider.Count > 0;
    }
}
