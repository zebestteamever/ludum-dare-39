﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAttackBehaviour : StateMachineBehaviour {

    // OnStateMachineEnter is called when entering a statemachine via its Entry Node
    override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    {
        animator.SetInteger("attackType", Random.Range(0, 2));
    }

}
