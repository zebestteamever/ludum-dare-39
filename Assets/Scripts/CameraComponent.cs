﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraComponent : MonoBehaviour {

    PlayerController[] players;
    Vector3 speed = Vector3.zero;

    // Use this for initialization
    void Start () {
        players = FindObjectsOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (players.Length == 0)
            return;

        Vector3 posSum;
        if (players.Length == 1)
        {
            posSum = players[0].transform.position;
        }
        else
        {
            posSum = Vector3.zero;
            foreach (PlayerController player in players)
            {
                posSum += player.transform.position;
            }
            posSum /= players.Length;
        }


        transform.position = Vector3.SmoothDamp(
                transform.position,
                new Vector3(posSum.x, posSum.y + 14f, posSum.z - 16f),
                ref speed, .3f);
    }

    public void Shake(float amplitude)
    {
        transform.position += Random.onUnitSphere * amplitude;
    }
}
