﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Component that ensures that the 2D game character is always facing the camera.
/// </summary>
public class Game2DCharacterViz : MonoBehaviour {

    /// <summary>
    /// <code>true</code> to flip the viz horizontally when facing left.
    /// </summary>
    public bool flipHorizontally = true;

    private GameObject viz;

    void Start()
    {
        viz = gameObject.FindTaggedChild("2DViz");
    }

    void Update () {
        if (viz)
        {
            Camera main = Camera.main;
            viz.transform.forward = main.transform.forward;
            viz.transform.rotation = Quaternion.identity;
            if (flipHorizontally)
            {
                Vector3 scale = viz.transform.localScale;
                scale.x = Vector3.Dot(main.transform.right, transform.forward) < 0 ? 1 : -1;
                viz.transform.localScale = scale;
            }
        }
	}
}
