﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// AI associated to a destructible power pillar.
/// </summary>
public class PillarAI : MonoBehaviour {

    public const int UnregisteredPillar = -1;

    public int pillarIndex = UnregisteredPillar;
    public PowerCableAI powerCable;

    private GameStateController gameStateController;
    private Damageable damageable;
    private DamageModifier damageModifier;
    private bool deathAck;
    private DamageParticleEmitter damageParticleEmitter;
    private ExplosionParticleEmitter explosionParticleEmitter;
    private MeshRenderer meshRenderer;

    void Start()
    {
        gameStateController = GameStateController.Instance;
        damageable = GetComponent<Damageable>();
        damageModifier = GetComponent<DamageModifier>();
        deathAck = false;
        damageParticleEmitter = GetComponentInChildren<DamageParticleEmitter>();
        explosionParticleEmitter = GetComponentInChildren<ExplosionParticleEmitter>();
        meshRenderer = GetComponent<MeshRenderer>();

        if (pillarIndex != UnregisteredPillar && gameStateController.PillarCount <= pillarIndex)
        {
            throw new InvalidProgramException("Invalid pillar identifier. The pillar index must be within the know "
                + "pillar indexes in the Game State Controller");
        }

        if (damageable)
        {
            damageable.Damage += Damageable_Damage;

            if (pillarIndex != UnregisteredPillar)
            {
                damageable.MaxHitPoints = gameStateController.GetPillarMaxHitPoints(pillarIndex);
                damageable.HitPoints = gameStateController.GetPillarHitPoints(pillarIndex);
            }
        }
    }

    void Update()
    {
        if (damageable && damageable.IsDead && !deathAck)
        {
            if (pillarIndex != UnregisteredPillar)
            {
                gameStateController.SetPillarHitPoints(pillarIndex, 0.0f);
            }
            if (damageModifier)
            {
                damageModifier.Active = false;
            }
            if (powerCable)
            {
                powerCable.TriggerPowerDown();
            }
            StartCoroutine(DeathAnimation());
            deathAck = true;
        }
    }

    private void Damageable_Damage(Damageable obj)
    {
        damageParticleEmitter.Play();
    }

    private void OnDestroy()
    {
        if (damageable != null)
        {
            damageable.Damage -= Damageable_Damage;
        }
    }

    private IEnumerator DeathAnimation()
    {
        explosionParticleEmitter.Play();
        yield return new WaitForSeconds(0.5f);
        meshRenderer.enabled = false;
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }

}
