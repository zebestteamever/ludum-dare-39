﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script that displays the enemy FOV.
/// </summary>
public class EnemyFOVMarker : MonoBehaviour {

    public GameObject fov;
    public float scale = 1.0f;
    public Color idleColor = Color.white;
    public Color alertColor = Color.red;
    public float colorTransition = 1.0f;

    private EnemyAI enemyAI;
    private GameObject personalFov;
    private SpriteRenderer spriteRenderer;
    private float appliedScale = 1.0f;
    private Color targetColor;
    private float targetColorTime;
    private Color awareColor;

    void Start () {
        enemyAI = GetComponent<EnemyAI>();
        personalFov = Instantiate(fov, transform);
        spriteRenderer = personalFov.GetComponent<SpriteRenderer>();
        spriteRenderer.color = idleColor;
        appliedScale = 1.0f;
        targetColor = idleColor;
        targetColorTime = 0.0f;
        awareColor = new Color(alertColor.r, alertColor.g, alertColor.b, 0.0f);
    }

    private void Update()
    {
        if (appliedScale != scale)
        {
            appliedScale = scale;
            personalFov.transform.localScale = fov.transform.localScale * appliedScale;
        }

        if (enemyAI)
        {
            Color newTargetColor = enemyAI.IsFullyAware ? awareColor : enemyAI.IsChasing ? alertColor : idleColor;
            if (targetColor != newTargetColor)
            {
                targetColor = newTargetColor;
                targetColorTime = Time.time + colorTransition;
            }

            if (spriteRenderer.color != targetColor)
            {
                spriteRenderer.color = Color.Lerp(spriteRenderer.color, targetColor, Mathf.Clamp(1 - (targetColorTime - Time.time) / colorTransition, 0.0f, 1.0f));
            }
        }
    }

}
