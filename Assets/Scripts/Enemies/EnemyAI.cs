﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour, Attacker
{

    private const int AIFSMLayer = 0;
    private static readonly int AlertTrigger = Animator.StringToHash("Alert");
    private static readonly int PatrollingState = Animator.StringToHash("Base Layer.Patrolling");
    private static readonly int ChasingState = Animator.StringToHash("Base Layer.Chasing");
    private static readonly int DyingState = Animator.StringToHash("Base Layer.Dying");

    public float fieldOfView = 110.0f;
    public float patrollingSpeed = 1.0f;
    public float chasingSpeed = 2.0f;
    public float poweredDamageMultiplier = 3.0f;
    public Transform[] waypoints;
    public bool facingLeft = true;
    public GameObject damageFxPrefab;
    public GameObject deathPrefab;
    public bool destroyParentOnDeath = false;

    private Animator animator;
    private Animator graphicAnimator;
    private NavMeshAgent navMeshAgent;
    private int waypointIndex;
    private GameObject targetPlayer;
    private bool playerInSight;
    private SphereCollider awareCollider;
    private LastPlayerSighting lastPlayerSighting;
    private bool fullyAware;
    private bool deathAck;
    private Damageable damageable;

    private Weapon weapon;

    public bool IsFullyAware
    {
        get
        {
            return fullyAware;
        }
    }

    public bool IsPatrolling
    {
        get
        {
            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(AIFSMLayer);
            return stateInfo.fullPathHash == PatrollingState;
        }
    }

    public bool IsChasing
    {
        get
        {
            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(AIFSMLayer);
            return stateInfo.fullPathHash == ChasingState;
        }
    }

    public bool IsDying
    {
        get
        {
            AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(AIFSMLayer);
            return stateInfo.fullPathHash == DyingState;
        }
    }

    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        targetPlayer = null;
        waypointIndex = 0;
        fullyAware = false;
        deathAck = false;

        animator = GetComponent<Animator>();
        if (!animator)
        {
            Debug.LogError("EnemyAlertAI requires an animator with the Enemy Animation Controller");
        }

        awareCollider = GetComponent<SphereCollider>();
        if (!awareCollider)
        {
            Debug.LogError("The EnemyAI requires a Sphere collider, which represents the enemy 'aware zone'.");
        }

        weapon = GetComponentInChildren<Weapon>();
        graphicAnimator = GetComponentsInChildren<Animator>()[1];
        damageable = GetComponent<Damageable>();
        damageable.Damage += Damageable_Damage;

        lastPlayerSighting = GameObject.FindGameObjectWithTag("GameController").GetComponent<LastPlayerSighting>();
    }

    private void OnDestroy()
    {
        damageable.Damage -= Damageable_Damage;
    }

    private void Damageable_Damage(Damageable obj)
    {
        SignalPlayerPresence();
        if (damageFxPrefab)
        {
            FXObject.Apply(damageFxPrefab, transform);
        }
    }

    void Update()
    {
        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(AIFSMLayer);
        if (stateInfo.fullPathHash == PatrollingState)
        {
            Patrolling();
        }
        else if (stateInfo.fullPathHash == ChasingState)
        {
            Chasing();
        }
        else if (stateInfo.fullPathHash == DyingState)
        {
            Dying();
        }

        if (weapon != null && weapon.CanAttack())
        {
            graphicAnimator.SetTrigger("attack");
        }

        if (Debug.isDebugBuild)
        {
            // Drawing FOV...
            Debug.DrawRay(transform.position, transform.forward, Color.white);
            Debug.DrawRay(transform.position, Quaternion.AngleAxis(-fieldOfView * .5f, transform.up) * transform.forward, Color.yellow);
            Debug.DrawRay(transform.position, Quaternion.AngleAxis(fieldOfView * .5f, transform.up) * transform.forward, Color.yellow);
        }
    }

    private void Patrolling()
    {
        if (waypoints.Length > 0)
        {
            navMeshAgent.speed = patrollingSpeed;
            if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
            {
                waypointIndex++;
                if (waypointIndex >= waypoints.Length)
                {
                    waypointIndex = 0;
                }
            }
            navMeshAgent.destination = waypoints[waypointIndex].transform.position;
        }
    }

    private void Chasing()
    {
        navMeshAgent.speed = chasingSpeed;
        navMeshAgent.destination = lastPlayerSighting.position;

        // If player is in sight, always face him/her.
        if (playerInSight)
        {
            Vector3 direction = targetPlayer.transform.position;
            direction.y = transform.transform.position.y;
            transform.LookAt(direction);
            navMeshAgent.updateRotation = false;
        } else
        {
            navMeshAgent.updateRotation = true;
        }

        fullyAware = !playerInSight && navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance;
    }

    private void Dying()
    {
        if (!deathAck)
        {
            navMeshAgent.isStopped = true;
            deathAck = true;
            graphicAnimator.SetBool("isDead", true);

            FXObject.Apply(deathPrefab, transform, () => {
                if (destroyParentOnDeath)
                {
                    Destroy(transform.parent.gameObject);
                }
                else
                {
                    Destroy(gameObject);
                }
            });
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject != targetPlayer && other.CompareTag("Player"))
        {
            GameObject potentialPlayer = other.gameObject;
            if (CanSeePlayer(potentialPlayer))
            {
                if (playerInSight)
                {
                    float playerDistance = Vector3.SqrMagnitude(targetPlayer.transform.position - transform.position);
                    float newPlayerDistance = Vector3.SqrMagnitude(potentialPlayer.transform.position - transform.position);

                    if (newPlayerDistance < playerDistance)
                    {
                        targetPlayer = potentialPlayer;
                    }
                } else
                {
                    targetPlayer = potentialPlayer;
                }
            }
        }

        if (other.gameObject == targetPlayer)
        {
            playerInSight = CanSeePlayer(targetPlayer);
            if (playerInSight)
            {

                lastPlayerSighting.SawPlayerAt(targetPlayer.transform.position, this);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == targetPlayer)
        {
            playerInSight = false;
            targetPlayer = null;
        }
    }

    private bool CanSeePlayer(GameObject player)
    {
        bool inSight;
        bool inFOV;

        Vector3 direction = player.transform.position - transform.position;

        if (fullyAware)
        {
            inFOV = true;
        } else
        {
            float angle = Vector3.Angle(transform.forward, direction);
            inFOV = angle < fieldOfView * .5f;
        }

        if (inFOV)
        {
            RaycastHit hit;
            int layerMask = ~(1 << (int)Layers.ENEMIES);
            if (Physics.Raycast(transform.position, direction.normalized, out hit, awareCollider.radius, layerMask))
            {
                inSight = hit.collider.gameObject == player;
            } else
            {
                inSight = false;
            }
        } else
        {
            inSight = false;
        }

        return inSight;
    }

    public void SignalPlayerPresence()
    {
        MusicController.Instance.StartFightMode();
        animator.SetTrigger(AlertTrigger);
    }

    void FixedUpdate()
    {
        if (navMeshAgent.velocity.y < -0.2f)
        {
            graphicAnimator.SetBool("isFalling", true);
        }
        else
        {
            graphicAnimator.SetBool("isFalling", false);
        }
        graphicAnimator.SetFloat("input_x", navMeshAgent.velocity.x);
        graphicAnimator.SetFloat("input_z", navMeshAgent.velocity.z);
    }

    public void StartBasicAttack()
    {
        StartCoroutine("DoAttack");
    }
    public void EndBasicAttack()
    {
        // Nothing to do
    }

    public void StartSpecialAttack()
    {
        // Nothing to do
    }

    public void EndSpecialAttack()
    {
        // Nothing to do
    }

    IEnumerator DoAttack()
    {
        yield return new WaitForSeconds(0.75f);
        if (damageable && damageable.UnderModifierInfluence)
        {
            weapon.PoweredAttack();
        }
        else
        {
            weapon.Attack();
        }
    }
}
