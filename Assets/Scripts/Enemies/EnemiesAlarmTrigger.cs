﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Trigger script that sends an alert event to enemies within the trigger whenever a player enters it.
/// </summary>
public class EnemiesAlarmTrigger : MonoBehaviour {

    private LastPlayerSighting lastPlayerSighting;

    void Start () {
        lastPlayerSighting = GameObject.FindGameObjectWithTag("GameController").GetComponent<LastPlayerSighting>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            lastPlayerSighting.SawPlayerAt(other.transform.position, transform.parent);
        }
    }
    
}
