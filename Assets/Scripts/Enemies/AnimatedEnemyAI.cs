﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimatedEnemyAI : MonoBehaviour {

    private Animator vizAnimator;
    private NavMeshAgent navMeshAgent;

    void Start ()
    {
        vizAnimator = gameObject.FindTaggedChild("2DViz").GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
    }
	
	void Update () {
        if (vizAnimator)
        {
            vizAnimator.SetBool("isWalking", navMeshAgent.velocity.sqrMagnitude > 0.0f);
        }
	}
}
