﻿using UnityEngine;
using System.Collections;

public enum Layers
{
    DECORS = 8,
    PLAYERS = 9,
    ENEMIES = 10,
}
