﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Script for players that handles:
/// - Player health regen
/// - Screenshake on damage
/// </summary>
public class PlayerAI : MonoBehaviour {

    private const float CameraShakeAmplitude = 2.0f;

    private GameStateController gameStateController;
    private Damageable damageable;
    private SpriteRenderer[] renderers;
    
	void Start () {
        damageable = GetComponent<Damageable>();
        gameStateController = GameStateController.Instance;
        renderers = GetComponentsInChildren<SpriteRenderer>();
        damageable.Damage += Damageable_Damage;
    }

    private void OnDestroy()
    {
        damageable.Damage -= Damageable_Damage;
    }

    private void Damageable_Damage(Damageable obj)
    {
        CameraComponent cameraComponent = Camera.main.GetComponent<CameraComponent>();
        if (cameraComponent)
        {
            cameraComponent.Shake(CameraShakeAmplitude);
        }
    }

    private void Update()
    {
        float relativeHealth = damageable.HitPoints / damageable.MaxHitPoints;
        float healthAnimation = 1.0f - (1.0f - relativeHealth) * Mathf.Cos(Time.time * 2 * Mathf.PI / 0.3f);

        renderers.ToList().ForEach(renderer =>
        {
            renderer.color = new Color(1.0f, healthAnimation, healthAnimation, 1.0f);
        });
    }

    private void FixedUpdate()
    {
        if (!damageable.IsDead)
        {
            damageable.HitPoints = Mathf.Min(damageable.HitPoints + gameStateController.playerRegenRate * Time.fixedDeltaTime, damageable.MaxHitPoints);
        }
    }
}
