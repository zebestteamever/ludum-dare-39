﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Represents the last player sighting by any enemy. Shared by all enemies within the scene.
/// </summary>
public class LastPlayerSighting : MonoBehaviour
{

    public Vector3 position = new Vector3();

    public void SawPlayerAt(Vector3 position, EnemyAI enemyAI)
    {
        if (this.position != position)
        {
            this.position = position;
            GameObject enemiesContrainer = enemyAI.transform.parent.parent.gameObject;
            SendSignal(enemiesContrainer);
        }
    }

    public void SawPlayerAt(Vector3 position, Transform enemiesContrainer)
    {
        SendSignal(enemiesContrainer.gameObject);
    }

    private static void SendSignal(GameObject enemiesContrainer)
    {
        enemiesContrainer.GetComponentsInChildren<EnemyAI>().Select(enemy => enemy.GetComponent<EnemyAI>())
                        .Where(enemy => enemy).ToList().ForEach(enemy => enemy.SignalPlayerPresence());
    }

}
