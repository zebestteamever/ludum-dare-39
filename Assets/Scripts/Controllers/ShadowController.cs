﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowController : MonoBehaviour {

    public Transform owner;

    private int decorsLayerMask;
    private SpriteRenderer spriteRenderer;

    // Use this for initialization
    void Start () {
        //get the mask to raycast against either the player or enemy layer
        decorsLayerMask = LayerMask.GetMask("Decors");
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {

    }

    void FixedUpdate()
    {
        Vector3 playerPosition = owner.transform.localPosition;
        Vector3 down = transform.TransformDirection(Vector3.down);
        RaycastHit hit;

        if (Physics.Raycast(owner.transform.position, Vector3.down, out hit, 10.0f, decorsLayerMask))
        {
            if (!spriteRenderer.enabled)
            {
                spriteRenderer.enabled = true;
            }
            transform.position = new Vector3(owner.transform.position.x, hit.point.y +0.2f, owner.transform.position.z);
        }
        else
        {
            spriteRenderer.enabled = false;
        }
    }
}
