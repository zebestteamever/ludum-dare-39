﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeController : MonoBehaviour {

    public bool open = false;
    public Transform door;
    public float openZValue = 0.055f;
    public float closeZValue = 0f;

    private bool opening = false;
    private bool closing = false;
    Vector3 speed = Vector3.zero;

    // Use this for initialization
    void Start () {
       door.localPosition = new Vector3(door.localPosition.x, door.localPosition.y, open ? openZValue : closeZValue);
    }
	
	// Update is called once per frame
	void Update () {
        if (opening)
        {
            opening = MoveDoor(openZValue, 0.5f);
        }
        if (closing)
        {
            closing = MoveDoor(closeZValue, 0.25f);
        }
    }

    private bool MoveDoor(float zTarget, float smoothTime)
    {
        door.localPosition = Vector3.SmoothDamp(
                        door.localPosition,
                        new Vector3(door.localPosition.x, door.localPosition.y, zTarget),
                        ref speed, smoothTime);
        if (door.localPosition.z == zTarget)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void Open()
    {
        opening = true;
        closing = false;
        speed = Vector3.zero;
    }
    public void Close()
    {
        opening = false;
        closing = true;
        speed = Vector3.zero;
    }
}
