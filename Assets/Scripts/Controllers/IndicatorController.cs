﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controller for the pillar indicator in the hub island.
/// </summary>
public class IndicatorController : MonoBehaviour {

    public const int UnregisteredPillar = -1;

    public int pillar = UnregisteredPillar;

    private GameObject on;
    private GameObject off;

    void Start () {
        on = transform.Find("on").gameObject;
        off = transform.Find("off").gameObject;
        off.SetActive(false);
    }
	
    public void SetIndicatorActive(bool isActive)
    {
        on.SetActive(isActive);
        off.SetActive(!isActive);
    }

}
