﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The game state controller script controls the persistent state of the game.
/// The associated GameState prefab must be placed on every scene.
/// The game state can be accessed through this class singleton.
/// </summary>
public class GameStateController : MonoBehaviour {

    public delegate void PillarStateChangedHandler(GameStateController controller, int pillar);

    private static GameStateController instance;

    public static GameStateController Instance
    {
        get
        {
            if (instance == null)
            {
                throw new InvalidOperationException("The singleton instance of GameStateController must be "
                    + "initialized by the Unity Engine (i.e. the GameState prefab must be on every scene)");
            }
            return instance;
        }
    }

    public float[] maxPillarHitPoints;
    public float playerRegenRate = 10.0f;

    public event PillarStateChangedHandler PillarStateChanged;

    private float[] pillarHitPoints;

    public int PillarCount
    {
        get
        {
            return maxPillarHitPoints.Length;
        }
    }

    private WorldData worldData;
    public GameObject player;

    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        } 
		else if (instance != this)
        {
            Destroy(gameObject);
        }

        instance.InitializePillarHitPOints();
        instance.InitWorldData();
    }

    void Start()
    {
        MusicController.Instance.StartStealthMode();
    }

    private void InitializePillarHitPOints()
    {
        pillarHitPoints = new float[maxPillarHitPoints.Length];
        Array.Copy(maxPillarHitPoints, pillarHitPoints, pillarHitPoints.Length);
    }

    public float GetPillarMaxHitPoints(int pillarIndex)
    {
        return maxPillarHitPoints[pillarIndex];
    }

    public float GetPillarHitPoints(int pillarIndex)
    {
        return pillarHitPoints[pillarIndex];
    }

    public void SetPillarHitPoints(int pillarIndex, float hitPoints)
    {
        float effectiveHP = Mathf.Clamp(hitPoints, 0, maxPillarHitPoints[pillarIndex]);
        float currentHP = pillarHitPoints[pillarIndex];
        if (currentHP != effectiveHP)
        {
            pillarHitPoints[pillarIndex] = effectiveHP;
            if (PillarStateChanged != null)
            {
                PillarStateChanged(this, pillarIndex);
            }
        }
    }

    private void InitWorldData()
    {
        worldData = WorldData.Instance;
        bool playerSpawned = false;
        if(worldData.activeIsland != "")
        {
            playerSpawned = SpawnPlayer(worldData.activeIsland, worldData.spawnIndex);
        }
        if (!playerSpawned)
        {
            worldData.activeIsland = "Island1";
            worldData.spawnIndex = 0;
            playerSpawned = SpawnPlayer(worldData.activeIsland, worldData.spawnIndex);
        }

        if (worldData.cinematicAlreadyPlayed)
        {
            GameObject.Find("GameController").GetComponent<Animator>().SetTrigger("bypassIntro"); // Will only work upon restart
        }
        else
        {
            worldData.cinematicAlreadyPlayed = true;
        }
    }

    private bool SpawnPlayer(String islandName, int spawnIndex)
    {
        GameObject islandSpawnGameObject = GameObject.Find(islandName+"/Spawns");
        if (islandSpawnGameObject != null)
        {
            Transform spawn = islandSpawnGameObject.transform.GetChild(spawnIndex);
            GameObject.Instantiate(player, spawn.position, Quaternion.identity);
            return true;
        }
        else
        {
            return false;
        }
    }
}
