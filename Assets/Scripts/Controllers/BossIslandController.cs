﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossIslandController : MonoBehaviour {

    public Damageable boss;

    private bool sendVictory = false;

    // Use this for initialization
    void Start () {
        PillarAI[] pillars = FindObjectsOfType<PillarAI>();
        foreach (PillarAI pillar in pillars)
        {
            DamageModifier modifier = pillar.GetComponent<DamageModifier>();
            boss.AddModifier(modifier);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (boss.IsDead && !sendVictory)
        {
            sendVictory = true;
            GameObject s = GameObject.FindGameObjectWithTag("GameController");
            Animator a = s.GetComponent<Animator>();
            a.enabled = true;
            a.SetTrigger("victory");

            SceneController sc = GameObject.FindObjectOfType<SceneController>();
            sc.Victory();
        }
	}
}
