﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour, Attacker
{

    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;

    public bool facingRight = true;
    private Vector3 moveDirection = Vector3.zero;

    private Animator animator;
    private CharacterController controller;
    private Weapon weapon;

    private bool isFalling = false;

    public AudioClip jumpSound;
    public AudioClip[] hitSounds;

    private AudioSource mainAudio;
    private AudioSource walkAudio;

    private int decorsLayerMask;

    // Use this for initialization
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        controller = GetComponent<CharacterController>();
        weapon = GetComponentInChildren<Weapon>();
        mainAudio = GetComponents<AudioSource>()[0];
        walkAudio = GetComponents<AudioSource>()[1];
        walkAudio.volume = 0;
        decorsLayerMask = LayerMask.GetMask("Decors");
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -10f)
        {
            GetComponent<Damageable>().Kill();
        }

        float horizontalValue = Input.GetAxis("Horizontal");
        float verticalValue = Input.GetAxis("Vertical");

        // Get the player controls
        float oldYDirection = moveDirection.y;
        moveDirection = new Vector3(horizontalValue, 0, verticalValue);
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection *= speed;

        // Test if can jump
        if (controller.isGrounded)
        {
            if (Input.GetButton("Jump") && !isFalling)
            {
                PlayClip(jumpSound, true);
                moveDirection.y = jumpSpeed;
                animator.SetTrigger("jump");
                isFalling = true;
            }
        }
        else
        {
            moveDirection.y = oldYDirection;
        }
        // Add gravity
        moveDirection.y -= gravity * Time.deltaTime;

        // Move the player
        controller.Move(moveDirection * Time.deltaTime);
        walkAudio.volume = moveDirection.magnitude > 2 ? 1 : 0;

        // Send the mouvement data to animator
        if (horizontalValue != 0 || verticalValue != 0)
        {
            animator.SetBool("isWalking", true);
            if (horizontalValue != 0)
            {
                animator.SetFloat("input_x", horizontalValue);
            }

            animator.SetFloat("input_z", verticalValue);
        }
        else
        {
            animator.SetBool("isWalking", false);
        }

        CombatControll();
    }

    public bool isAlive()
    {
        return !GetComponent<Damageable>().IsDead;
    }

    private void CombatControll()
    {
        // Combat
        if (Input.GetButton("attackSpecial"))
        {
            animator.SetBool("attackSpecial", true);
        }
        else
        {
            animator.SetBool("attackSpecial", false);
            if (Input.GetButtonDown("attack"))
            {
                animator.SetTrigger("attack");
            }
        }
    }

    void FixedUpdate()
    {
        float h = Input.GetAxis("Horizontal");
        if (h > 0 && !facingRight)
            Flip();
        else if (h < 0 && facingRight)
            Flip();

        if (!controller.isGrounded && controller.velocity.y < -3f)
        {
            // Pour éviter les glitchs, on s'assure d'avoir au moins 2 frames avec une chute avant de lancer l'anim
            if (isFalling)
            {
                animator.SetBool("isFalling", true);
            }
        }
        else if (isFalling)
        {
            animator.SetBool("isFalling", false);

            RaycastHit hit;
            if (Physics.Raycast(transform.position, Vector3.down, out hit, 10.0f, decorsLayerMask))
            {
                string hitName = hit.collider.gameObject.name.ToLower();
                if (hitName.Contains("island") || hitName.Contains("castle"))
                {
                    isFalling = false;
                }
            }
        }
        animator.SetBool("isGrounded", controller.isGrounded);
    }
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void StartBasicAttack()
    {
        StartCoroutine("DoAttack");
    }
    public void EndBasicAttack()
    {
        // Nothing to do
    }

    public void StartSpecialAttack()
    {
        StartCoroutine("DoSpecialAttack");
    }

    public void EndSpecialAttack()
    {
        StopCoroutine("DoSpecialAttack");
    }

    IEnumerator DoSpecialAttack()
    {
        for (;;)
        {
            weapon.Attack(10f);
            PlayRandomHitClip();
            yield return new WaitForSeconds(0.25f);
        }
    }

    IEnumerator DoAttack()
    {
        yield return new WaitForSeconds(0.20f);
        PlayRandomHitClip();
        weapon.Attack();
    }

    public void PlayRandomHitClip()
    {
        AudioClip clip = hitSounds[Random.Range(0, hitSounds.Length)];
        PlayClip(clip, true);
    }

    public void PlayClip(AudioClip clip, bool randomPitch)
    {
        mainAudio.clip = clip;
        mainAudio.pitch = randomPitch ? Random.Range(0.8f, 1.2f) : 1f;
        mainAudio.Play();
    }
}
    
