﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Handles the global game scenes, including the hud.
/// </summary>
public class SceneController : MonoBehaviour {

    private GameStateController gameStateController;
    private HUDInfoController hudInfo;
    private ILookup<int, IndicatorController> indicators;
    PlayerController[] players;

    private const float MENU_BLACK_OPACITY = .5f;

    private float targetBlackScreenOpacity = 0;
    private Image blackScreen;
    private float deathOpacitySpeed = 0;

    private GameObject pauseMenu;
    private GameObject victoryMenu;

    private bool victory = false;

    void Start()
    {
        gameStateController = GameStateController.Instance;
        hudInfo = gameObject.GetComponentInChildren<HUDInfoController>();
        indicators = GameObject.FindObjectsOfType<IndicatorController>()
            .ToLookup(pillar => pillar.pillar);

        gameStateController.PillarStateChanged += GameStateController_PillarStateChanged;
        StartCoroutine(ShowHUDUpdateAsync());

        blackScreen = transform.Find("HUDCanvas").Find("BlackScreen").GetComponent<Image>();
        blackScreen.color = Color.black;

        victoryMenu = transform.Find("HUDCanvas").Find("VictoryMenu").gameObject;
    }

    public void Victory()
    {
        victoryMenu.SetActive(true);
        victory = true;
    }

    public void ShowMenu()
    {
        targetBlackScreenOpacity = MENU_BLACK_OPACITY;
        pauseMenu.SetActive(true);
        victoryMenu.SetActive(victory);
    }

    public void ExitMenu()
    {
        targetBlackScreenOpacity = 0f;
        pauseMenu.SetActive(false);
        victoryMenu.SetActive(false);
    }
    
    public void Exit()
    {
        Application.Quit();
    }

    public void GoBackToLastCheckpoint()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Update()
    {
        if (AllPlayersDead())
        {
            StartCoroutine(RestartScene());
        }

        if (targetBlackScreenOpacity != blackScreen.color.a)
        {
            float newBlackScreenOpacity = Mathf.SmoothDamp(blackScreen.color.a, targetBlackScreenOpacity, ref deathOpacitySpeed, targetBlackScreenOpacity == MENU_BLACK_OPACITY ? 0.3f : 1f);
            blackScreen.color = new Color(blackScreen.color.r, blackScreen.color.g, blackScreen.color.b, newBlackScreenOpacity);
        }
        if (Input.GetButtonDown("Cancel"))
        {
            if (targetBlackScreenOpacity != MENU_BLACK_OPACITY)
            {
                ShowMenu();
            }
            else
            {
                ExitMenu();
            }
        }
        pauseMenu = transform.Find("HUDCanvas").Find("PauseMenu").gameObject;
    }

    public IEnumerator RestartScene()
    {
        pauseMenu.SetActive(false);
        targetBlackScreenOpacity = 1f;
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private bool AllPlayersDead()
    {
        players = FindObjectsOfType<PlayerController>();
        foreach (PlayerController player in players){
            if (player.isAlive())
            {
                return false;
            }
        }
        return true;
    }

    private void GameStateController_PillarStateChanged(GameStateController controller, int pillar)
    {
        Debug.LogFormat("HUD: notified for change wiht pillar {0}", pillar);
        if (controller.GetPillarHitPoints(pillar) <= 0.0f)
        {
            // A power pillar has been destroyed.
            indicators[pillar].ToList().ForEach(indicator => indicator.SetIndicatorActive(false));
            ShowHUDUpdate();
        }
    }

    void OnDestroy()
    {
        gameStateController.PillarStateChanged -= GameStateController_PillarStateChanged;
    }

    public void ShowHUDUpdate()
    {
        hudInfo.ShowHUD();
    }

    public IEnumerator ShowHUDUpdateAsync()
    {
        yield return null;
        ShowHUDUpdate();
    }
}
