﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Controller for the main info HUD.
/// </summary>
public class HUDInfoController : MonoBehaviour {

    public Animator[] pillarIcons;

    private GameStateController gameStateController;
    private Animator animator;

	void Start () {
        animator = GetComponent<Animator>();
        gameStateController = GameStateController.Instance;
    }

    private void UpdateState()
    {
        int pillarCount = Enumerable.Range(0, gameStateController.PillarCount)
            .Where(pillar => gameStateController.GetPillarHitPoints(pillar) > 0.0f).Count();
        SetPillarCount(pillarCount);
    }
	
	public void ShowHUD()
    {
        UpdateState();
        animator.SetTrigger("Update");
    }

    public void SetPillarCount(int count)
    {
        for (int i = 0; i < pillarIcons.Length; i++)
        {
            bool isOn = pillarIcons[i].GetCurrentAnimatorStateInfo(0).IsName("On");
            bool mustBeOn = i < count;
            if (isOn != mustBeOn)
            {
                pillarIcons[i].SetTrigger(mustBeOn ?  "on" : "off");
            }
        }
    }

}
