﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IslandController : MonoBehaviour {

    public GameObject pillarsContainer;
    public GameObject enemiesContainer;
    public GameObject spawnContainer;
    public BridgeController[] bridges;
    public bool activeIsland = false;

    public int islandIndex;

    private WorldData worldData;
    private Damageable[] enemies;
    private Damageable[] pillars;
    private List<Transform> spawns;
    private bool activePillar = true;
    private bool activeEnemy = true;

    // Use this for initialization
    void Start()
    {
        enemies = enemiesContainer.GetComponentsInChildren<Damageable>();
        pillars = pillarsContainer.GetComponentsInChildren<Damageable>();
        if (spawnContainer != null)
        {
            spawns = GameObjectUtils.GetFirstLevelChildren(spawnContainer.transform);
        }
        activeEnemy = !AllDead(enemies);
        activePillar = !AllDead(pillars);

        foreach (Damageable pillar in pillars)
        {
            DamageModifier modifier = pillar.GetComponent<DamageModifier>();
            foreach (Damageable enemy in enemies)
            {
                enemy.AddModifier(modifier);
            }
        }
        worldData = WorldData.Instance;
        WorldData.IslandData islandData = worldData.GetIslandData(gameObject.name);
        if(islandData == null)
        {
            worldData.InitIsland(gameObject.name, !activePillar, !activeEnemy);
        }
        else
        {
            if (islandData.enemiesDead)
            {
                foreach (Damageable enemy in enemies)
                {
                    enemy.Kill();
                    enemy.enabled = false;
                }
                activeEnemy = false;
                OpenBriges();
            }
            if (islandData.pillarsDead)
            {
                foreach (Damageable pillar in pillars)
                {
                    pillar.Kill();
                    pillar.enabled = false;
                }
                activePillar = false;
            }
        }
    }

   
    // Update is called once per frame
    void Update () {
        if (activeIsland)
        {
            if (activePillar && AllDead(pillars))
            {
                worldData.DestroyPillar(gameObject.name);
                activePillar = false;
                // TODO send message to enemy to remove shield
                if (!activeEnemy)
                {
                    OpenBriges();
                }
            }
            if (activeEnemy && AllDead(enemies))
            {
                worldData.DestroyEnemies(gameObject.name);
                activeEnemy = false;
                MusicController.Instance.StartStealthMode();
                OpenBriges();
            }
        }
    }

    private void CloseBridges()
    {
        foreach (BridgeController bridge in bridges)
        {
            bridge.Close();
        }
    }

    private void OpenBriges()
    {
        foreach (BridgeController bridge in bridges)
        {
            bridge.Open();
        }
    }

    private bool AllDead(Damageable[] damageables)
    {
        if(pillars == null) { return true; }
        bool alive = false;
        foreach(Damageable damageable in damageables)
        {
            if (!damageable.IsDead)
            {
                alive = true;
                break;
            }
        }
        return !alive;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (!activeIsland)
            {
                activeIsland = true;
                worldData.activeIsland = gameObject.name;
                if (activeEnemy)
                {
                    CloseBridges();
                }
                MusicController.Instance.StartStealthMode();
            }
            // Even if it's the same island we choose the best spawn point again
            worldData.spawnIndex = NearestSpawnPoint(other.transform);
        }
    }

    private int NearestSpawnPoint(Transform player)
    {
        float minDist = 1000;
        int index = 0;
        for(int i = 0; i < spawns.Count; i++)
        {
            float currentDist = Vector3.Distance(spawns[i].position, player.position);
            if (currentDist < minDist)
            {
                minDist = currentDist;
                index = i;
            }
        }
        return index;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            activeIsland = false;
        }
    }
}
