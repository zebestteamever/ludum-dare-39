﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class GameObjectUtils
{

    public static GameObject FindChild(this GameObject gameObject, Func<GameObject, bool> predicate)
    {
        return Enumerable.Range(0, gameObject.transform.childCount)
            .Select(index => gameObject.transform.GetChild(index).gameObject).FirstOrDefault(predicate);
    }

    public static GameObject FindTaggedChild(this GameObject gameObject, string tag)
    {
        return gameObject.FindChild(child => child.CompareTag(tag));
    }

    public static List<Transform> GetFirstLevelChildren(this Transform transform)
    {
        List<Transform> children = new List<Transform>();
        for (int i = 0; i < transform.childCount; i++)
        {
            children.Add(transform.GetChild(i));
        }
        return children;
    }

}