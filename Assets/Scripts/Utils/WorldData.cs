﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class WorldData : MonoBehaviour
{
    public string activeIsland;
    public int spawnIndex = 0;
    public bool cinematicAlreadyPlayed = false;

    public KeyValuePairLists<IslandData> islands = new KeyValuePairLists<IslandData>();

    [Serializable]
    public class IslandData
    {
        public bool pillarsDead;
        public bool enemiesDead;
    }

    private static WorldData instance;

    public static WorldData Instance
    {
        get
        {
            if (instance == null)
            {
                throw new InvalidOperationException("The singleton instance of WorldData must be "
                    + "initialized by the Unity Engine (i.e. the GameState prefab must be on every scene)");
            }
            return instance;
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public IslandData GetIslandData(string islandName)
    {
        IslandData value = new IslandData();
        if (islands.TryGetValue(islandName, ref value))
        {
            return value;
        }
        else
        {
            return null;
        }
    }

    public void InitIsland(string islandName, bool pillarsDead, bool enemiesDead)
    {
        IslandData value = new IslandData();
        value.pillarsDead = pillarsDead;
        value.enemiesDead = enemiesDead;
        islands.TrySetValue(islandName, value);
    }

    public void EnterIsland(string islandName, int spawn)
    {
        activeIsland = islandName;
        spawnIndex = spawn;
    }

    public void DestroyEnemies(string islandName)
    {
        IslandData value = new IslandData();
        if (islands.TryGetValue(islandName, ref value)) {
            value.enemiesDead = true;
        }            
    }

    public void DestroyPillar(string islandName)
    {
        IslandData value = new IslandData();
        if (islands.TryGetValue(islandName, ref value))
        {
            value.pillarsDead = true;
        }
    }

    [Serializable]
    public class KeyValuePairLists<T>
    {
        public List<string> keys = new List<string>();
        public List<T> values = new List<T>();
        public void Clear()
        {
            keys.Clear();
            values.Clear();
        }
        public void TrySetValue(string key, T value)
        {
            int index = keys.FindIndex(x => x == key);
            if (index > -1)
            {
                values[index] = value;
            }
            else
            {
                keys.Add(key);
                values.Add(value);
            }
        }
        public bool TryGetValue(string key, ref T value)
        {
            int index = keys.FindIndex(x => x == key);
            if (index > -1)
            {
                value = values[index];
                return true;
            }
            return false;
        }
    }
}
