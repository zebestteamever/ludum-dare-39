﻿using System;
using UnityEngine;
using UnityEngine.Audio;

public class MusicController : MonoBehaviour {

    public AudioMixer mixer;

    public AudioSource fightSource;

    public bool fightMode = false;

    private float volumeVelocity1 = 0;
    private float volumeVelocity2 = 0;

    private float defaultStealthVolume = 3;

    private static MusicController instance;

    public static MusicController Instance
    {
        get
        {
            if (instance == null)
            {
                throw new InvalidOperationException("The singleton instance of MusicController must be "
                    + "initialized by the Unity Engine (i.e. the GameState prefab must be on every scene)");
            }
            return instance;
        }
    }

    void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        fightMode = false;
        instance.fightMode = false;
    }

    void Start ()
    {
        mixer.GetFloat("StealthVol", out defaultStealthVolume);
        fightMode = false;
        instance.fightMode = false;
    }
	
	void Update () {
        float targetVolume = fightMode ? -100 : defaultStealthVolume;
        float currentVolume;
        mixer.GetFloat("StealthVol", out currentVolume);
        mixer.SetFloat("StealthVol", Mathf.SmoothDamp(currentVolume, targetVolume, ref volumeVelocity1, fightMode ? 1f : 1f));

        targetVolume = fightMode ? 0 : -100;
        mixer.GetFloat("FightVol", out currentVolume);
        mixer.SetFloat("FightVol", Mathf.SmoothDamp(currentVolume, targetVolume, ref volumeVelocity2, fightMode ? 0 : 5f));
        if (!fightMode && currentVolume < -50 && fightSource.isPlaying)
        {
            fightSource.Stop();
        } 
    }

    public void StartFightMode()
    {
        if (!fightMode)
        {
            fightMode = true;
            fightSource.Play();
        }
    }

    public void StartStealthMode()
    {
        fightMode = false;
    }
}
