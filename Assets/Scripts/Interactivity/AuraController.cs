﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script controlling the damageable aura.
/// </summary>
public class AuraController : MonoBehaviour {

    private Animator animator;
    private Damageable damageable;
    
	void Start () {
        animator = GetComponent<Animator>();
        damageable = GetComponentInParent<Damageable>();
        damageable.PropertyChanged += Damageable_PropertyChanged;
        animator.SetBool("Active", damageable.UnderModifierInfluence);
    }

    private void Damageable_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        if (e.PropertyName == "UnderModifierInfluence")
        {
            animator.SetBool("Active", damageable.UnderModifierInfluence);
        }
    }

    void OnDestroy()
    {
        damageable.PropertyChanged -= Damageable_PropertyChanged;
    }
}
