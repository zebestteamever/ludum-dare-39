﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEngine;

/// <summary>
/// Component for game objects that can take damage.
/// </summary>
public class Damageable : MonoBehaviour, INotifyPropertyChanged
{

    public float maxHitPoints = 100.0f;
    public DamageModifier[] modifiers;

    private float hitPoints;
    private bool dead;

    #region PropertyChanged

    public event PropertyChangedEventHandler PropertyChanged;

    protected void OnPropertyChanged(PropertyChangedEventArgs e)
    {
        PropertyChangedEventHandler handler = PropertyChanged;
        if (handler != null)
            handler(this, e);
    }

    protected void OnPropertyChanged(string propertyName)
    {
        OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
    }

    #endregion

    #region UnderModifierInfluence

    private bool underModifierInfluence = false;
    public bool UnderModifierInfluence
    {
        get
        {
            return underModifierInfluence;
        }
        set
        {
            if (underModifierInfluence != value)
            {
                underModifierInfluence = value;
                OnPropertyChanged("UnderModifierInfluence");
            }
        }
    }

    private bool IsUnderModifierInfluence()
    {
        return modifiers.Count() > 0 && modifiers.Any(modifier => modifier.Active);
    }

    #endregion

    #region IsDead

    public bool IsDead { get { return dead; } }

    #endregion

    #region MaxHitPoints

    public float MaxHitPoints
    {
        get
        {
            return maxHitPoints;
        }
        set
        {
            if (value < 0.0f)
            {
                throw new ArgumentOutOfRangeException("value", "Max hit points should be positive");
            }
            maxHitPoints = value;
        }
    }

    #endregion

    #region HitPoints

    public float HitPoints
    {
        get
        {
            return hitPoints;
        }
        set
        {
            if (IsDead)
            {
                throw new InvalidOperationException("Damageable is dead");
            }
            hitPoints = value;
            CheckForDeath();
        }
    }

    #endregion

    #region Death event

    public event Action<Damageable> Death;

    private void TriggerDeathEvent()
    {
        if (Death != null)
        {
            Death(this);
        }
    }

    #endregion

    #region Damage event

    public event Action<Damageable> Damage;

    private void TriggerDamageEvent()
    {
        if (Damage != null)
        {
            Damage(this);
        }
    }

    #endregion

    void Start()
    {
        hitPoints = maxHitPoints;
        dead = false;
        UnderModifierInfluence = IsUnderModifierInfluence();
        modifiers.ToList().ForEach(modifier => modifier.PropertyChanged += Modifier_PropertyChanged);
    }

    private void OnDestroy()
    {
        modifiers.ToList().ForEach(modifier => modifier.PropertyChanged -= Modifier_PropertyChanged);
    }

    private void Modifier_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
        if (e.PropertyName == "Active")
        {
            UnderModifierInfluence = IsUnderModifierInfluence();
        }
    }

    /// <summary>
    /// Deals some damage to the game objects.
    /// The provided damage amount will be processed to take into account the game object's shield.
    /// </summary>
    /// <param name="source">Object that has dealt the damage</param>
    /// <param name="rawDamage">Amount of damage to deal in hit points</param>
    public void TakeDamage(GameObject source, float rawDamage)
    {
        float effectiveDamage = ComputeEffectiveDamage(source, rawDamage);
        Debug.Log(source.name + " attack " + gameObject.name + " with " + rawDamage + " damages. Effective damage: " + effectiveDamage);
        hitPoints -= effectiveDamage;
        DoForEachAnimator(animator =>
        {
            if (animator.parameters.Any(param => param.name == "takeDamage"))
            {
                animator.SetTrigger("takeDamage");
            }
        });
        TriggerDamageEvent();
        CheckForDeath();
    }

    /// <summary>
    /// Checks whether the damageable is dead, in which case the death handler will be called (only once).
    /// </summary>
    private void CheckForDeath()
    {
        if (hitPoints <= 0.0f && !dead)
        {
            Kill();
        }
    }

    /// <summary>
    /// Computes the effective amount of damage to the current damageable object, taking into account any shield
    /// provided by a power pillar.
    /// </summary>
    /// <param name="source">Object that has dealt the damage</param>
    /// <param name="rawDamage">Amount of raw damage</param>
    /// <returns>Amount of effective damage</returns>
    private float ComputeEffectiveDamage(GameObject source, float rawDamage)
    {
        return modifiers.ToList().Aggregate(rawDamage, (damage, modifier) => modifier.DampenDamage(source, gameObject, rawDamage, damage));
    }

    /// <summary>
    /// Called when then damageable objects just died.
    /// </summary>
    private void HandleDeath()
    {
        DoForEachAnimator(animator => {
            if (animator.parameters.Any(param => param.name == "isDead"))
            {
                animator.SetBool("isDead", true);
            }
        });
        DoForEachSpriteRenderer(sr => {
            sr.sprite = null;
        });
        if (gameObject.name == "pillar")
        {
            GetComponent<AudioSource>().Play();
        }

        TriggerDeathEvent();
    }

    public void AddModifier(DamageModifier modifier)
    {
        Array.Resize(ref modifiers, modifiers.Length + 1);
        modifiers[modifiers.Length - 1] = modifier;
        modifier.PropertyChanged += Modifier_PropertyChanged;
        UnderModifierInfluence = IsUnderModifierInfluence();
    }

    public void Kill()
    {
        if (!dead)
        {
            Debug.Log(gameObject.name + " is dead");
            dead = true;
            HandleDeath();
        }
    }

    private void DoForEachAnimator(Action<Animator> handler)
    {
        Animator[] animators = GetComponentsInChildren<Animator>();
        foreach (var animator in animators)
        {
            handler(animator);
        }
    }

    private void DoForEachSpriteRenderer(Action<SpriteRenderer> handler)
    {
        SpriteRenderer[] srs = GetComponentsInChildren<SpriteRenderer>();
        foreach (var sr in srs)
        {
            handler(sr);
        }
    }
}
