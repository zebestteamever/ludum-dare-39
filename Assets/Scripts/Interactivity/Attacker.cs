﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Component for game objects that can attack
/// </summary>
public interface Attacker {

    void StartBasicAttack();
    void EndBasicAttack();

    void StartSpecialAttack();
    void EndSpecialAttack();
}
