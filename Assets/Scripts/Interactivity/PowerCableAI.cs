﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Script associated with a power pillar.
/// </summary>
public class PowerCableAI : MonoBehaviour {

    private Animator animator;

	void Start () {
        animator = GetComponent<Animator>();
	}

    public void TriggerPowerDown()
    {
        animator.SetTrigger("PowerDown");
    }
}
