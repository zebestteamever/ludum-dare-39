﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

/// <summary>
/// Component that has an effect on damage dealt.
/// </summary>
public class DamageModifier : MonoBehaviour, INotifyPropertyChanged {

    public bool initiallyActive = true;
    public float damageFactor = 0.3f;

    #region PropertyChanged

    public event PropertyChangedEventHandler PropertyChanged;

    protected void OnPropertyChanged(PropertyChangedEventArgs e)
    {
        PropertyChangedEventHandler handler = PropertyChanged;
        if (handler != null)
            handler(this, e);
    }

    protected void OnPropertyChanged(string propertyName)
    {
        OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
    }

    #endregion

    #region Active

    private bool active;
    public bool Active {
        get
        {
            return active;
        }
        set
        {
            if (active != value)
            {
                active = value;
                OnPropertyChanged("Active");
            }
        }
    }

    #endregion

    private void Start()
    {
        Active = initiallyActive;
    }

    /// <summary>
    /// Computes the dampening effect of the damage modifier.
    /// </summary>
    /// <param name="source">Object that has dealt the damage</param>
    /// <param name="target">Object that received the damage</param>
    /// <param name="rawDamage">Original raw damage</param>
    /// <param name="dampenDamage">Current dampen damage</param>
    /// <returns>Damage dampened by this modifier</returns>
    public float DampenDamage(GameObject source, GameObject target, float rawDamage, float dampenDamage)
    {
        return Active ? dampenDamage * damageFactor : dampenDamage;
    }

}
