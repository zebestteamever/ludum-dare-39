﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionParticleEmitter : MonoBehaviour {

    private ParticleSystem particleSystem;
    
	void Start () {
        particleSystem = GetComponent<ParticleSystem>();
	}
	
	public void Play()
    {
        particleSystem.Play();
    }
}
