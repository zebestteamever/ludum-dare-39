﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents a prefab that handles a particular FX.
/// The FX is started and then self-destructs upon completion.
/// </summary>
public class FXObject : MonoBehaviour {

    private new ParticleSystem particleSystem;
    private bool initialized;

    private Action fxCompletionHandler;

    #region Static Apply

    public static FXObject Apply(GameObject prefab, Transform parent)
    {
        return Apply(prefab, parent, null);
    }

    public static FXObject Apply(GameObject prefab, Transform parent, Action fxCompletionHandler)
    {
        FXObject fx = GameObject.Instantiate(prefab, parent).GetComponent<FXObject>();
        fx.fxCompletionHandler = fxCompletionHandler;
        return fx;
    }

    #endregion

    void Start () {
        particleSystem = GetComponent<ParticleSystem>();
        if (!particleSystem)
        {
            throw new InvalidOperationException("A particle system is required");
        }

        initialized = false;
    }

    private void Update()
    {
        if (initialized)
        {
            if (particleSystem.isStopped)
            {
                if (fxCompletionHandler != null)
                {
                    fxCompletionHandler();
                }

                Destroy(gameObject);
            }
        }
        else
        {
            initialized = true;
            particleSystem.Play();
        }
    }

}
