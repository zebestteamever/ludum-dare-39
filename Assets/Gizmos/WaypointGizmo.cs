﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointGizmo : MonoBehaviour {

    private void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position, "wayPoint.png", true);
    }
}
