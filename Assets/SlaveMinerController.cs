﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlaveMinerController : MonoBehaviour {

    public bool mine = true;

	void Start () {
        if (mine)
        {
            StartCoroutine(MineInRandomTime());
        }
	}

    IEnumerator MineInRandomTime()
    {
        yield return new WaitForSeconds(Random.Range(0f, 3f));
        GetComponent<Animator>().SetTrigger("mine");
    }
	
}
